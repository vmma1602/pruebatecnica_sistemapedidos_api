DROP DATABASE sistema_pedidos;
CREATE DATABASE sistema_pedidos;
USE sistema_pedidos;

CREATE TABLE roles(
idRol INT PRIMARY KEY IDENTITY(1,1),
nombreRol VARCHAR(50));

CREATE TABLE usuarios(
idUsuario INT PRIMARY KEY IDENTITY(1,1),
nombreUsuario VARCHAR(50) NOT NULL,
usuarioAcceso VARCHAR(50) NOT NULL,
claveAcceso VARCHAR(50) NOT NULL,
fkRol  INT NOT NULL,
FOREIGN KEY (fkRol) REFERENCES roles(idRol));

CREATE TABLE productos(
idProducto INT PRIMARY KEY IDENTITY(1,1),
claveProducto VARCHAR(100) NOT NULL UNIQUE,
nombreProducto VARCHAR(100) NOT NULL,
descripcion VARCHAR(255) NOT NULL,
precioUnitario FLOAT);

CREATE TABLE clientes(
idCliente INT PRIMARY KEY IDENTITY(1,1),
nombreCliente VARCHAR(100) NOT NULL);

CREATE TABLE existencias(
idExistencia INT PRIMARY KEY IDENTITY(1,1),
unidadDeMedida VARCHAR(20) NOT NULL,
stock FLOAT,
fkProducto INT NOT NULL,
FOREIGN KEY(fkProducto) REFERENCES productos(idProducto));

CREATE TABLE pedidos(
idPedido INT PRIMARY KEY IDENTITY(1,1),
cantidad FLOAT,
estatus VARCHAR(20) ,
fkProducto INT NOT NULL,
fkCliente INT NOT NULL,
FOREIGN KEY(fkProducto) REFERENCES productos(idProducto),
FOREIGN KEY(fkCliente) REFERENCES clientes(idCliente));

--Carga de datos
INSERT INTO roles VALUES ('Administrador'),('Vendedor'),('Administrativo');
INSERT INTO usuarios VALUES ('Administrador','admin','1234', 1),('Usuario administrativo','administrativo','1234', 3),('Usuario vendedor','vendedor','1234', 2)
INSERT INTO clientes VALUES ('Cliente 01'),('Cliente 02'),('Cliente 03');

--Trigger para ingresar existencia automaticamente al crear un producto
CREATE TRIGGER crearExistenciaTrigger ON [productos]
AFTER INSERT
AS
INSERT INTO existencias VALUES('Por definir',0, (select ins.idProducto FROM INSERTED ins));

--Probar el trigger
INSERT INTO productos VALUES ('00001','Coca cola','Coca cola de 600 ml',15);
