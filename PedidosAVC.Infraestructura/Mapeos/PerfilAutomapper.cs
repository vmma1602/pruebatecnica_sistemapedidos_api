﻿using AutoMapper;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Infraestructura.Mapeos
{
    public class PerfilAutomapper : Profile
    {
        public PerfilAutomapper()
        {
            CreateMap<Usuario, UsuarioDto>().ReverseMap();
            CreateMap<UsuarioRol, UsuarioDto>().ReverseMap();
            CreateMap<Role, RolDto>().ReverseMap();
            CreateMap<Cliente, ClienteDto>().ReverseMap();
            CreateMap<Producto, ProductoDto>().ReverseMap();
            CreateMap<ProductoExistencia, ProductoExistenciaDto>().ReverseMap();
            CreateMap<Existencia, ProductoExistenciaDto>().ReverseMap();
            CreateMap<PedidoProducto, PedidoProductoDto>().ReverseMap();
            CreateMap<Pedido, PedidoDto>().ReverseMap();
        }
    }
}
