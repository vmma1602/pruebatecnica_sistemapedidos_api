﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Infraestructura.Datos;

namespace PedidosAVC.Infraestructura.Repos
{
    public class ClienteRepo : IClienteRepo
    {
        private readonly PedidosDbContext _context;
        public ClienteRepo(PedidosDbContext context)
        {
            _context = context;
        }
        public async Task<IList<Cliente>> DevolverClientes()
        {
            return await _context.Clientes.ToListAsync();
        }
    }
}
