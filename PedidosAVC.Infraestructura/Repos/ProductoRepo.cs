﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Infraestructura.Datos;

namespace PedidosAVC.Infraestructura.Repos
{
    public class ProductoRepo : IProductoRepo
    {
        private readonly PedidosDbContext _context;
        public ProductoRepo(PedidosDbContext context)
        {
            _context = context;
        }
        public async Task<Producto> AgregarProducto(Producto producto)
        {
            await _context.Productos.AddAsync(producto);
            await _context.SaveChangesAsync();
            int ultimoId = _context.Productos.Max(u => u.IdProducto);
            return await _context.Productos.Where(u => u.IdProducto == ultimoId).FirstOrDefaultAsync();
        }

        public async Task<IList<Producto>> DevolverProductos()
        {
            return await _context.Productos.ToListAsync();
        }

        public async Task<Producto> EditarProducto(Producto producto, int productoId)
        {
            if (producto == null) return null;

            producto.IdProducto = productoId;
            _context.Productos.Attach(producto);
            if (!string.IsNullOrEmpty(producto.ClaveProducto))
            {
                _context.Entry(producto).Property(u => u.ClaveProducto).IsModified = true;
            }

            if (!string.IsNullOrEmpty(producto.NombreProducto))
            {
                _context.Entry(producto).Property(u => u.NombreProducto).IsModified = true;
            }

            if (!string.IsNullOrEmpty(producto.Descripcion))
            {
                _context.Entry(producto).Property(u => u.Descripcion).IsModified = true;
            }

            if (producto.PrecioUnitario > 0)
            {
                _context.Entry(producto).Property(u => u.PrecioUnitario).IsModified = true;
            }

            await _context.SaveChangesAsync();
            return producto;
        }
    }
}
