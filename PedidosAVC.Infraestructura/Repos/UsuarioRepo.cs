﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Infraestructura.Datos;

namespace PedidosAVC.Infraestructura.Repos
{
    public class UsuarioRepo : IUsuarioRepo
    {
        private readonly PedidosDbContext _context;
        public UsuarioRepo(PedidosDbContext context)
        {
            _context = context;
        }

        public async Task<Usuario> AgregarUsuario(Usuario usuario)
        {
            await _context.Usuarios.AddAsync(usuario);
            await _context.SaveChangesAsync();
            int ultimoId = _context.Usuarios.Max(u => u.IdUsuario);
            return await _context.Usuarios.Where(u => u.IdUsuario == ultimoId).FirstOrDefaultAsync();
        }

        public async Task<UsuarioRol> BuscarUsuarioParaLogin(UsuarioDto usuarioDto)
        {
            return await (from usuario in _context.Usuarios
                          join rol in _context.Roles on usuario.FkRol equals rol.IdRol
                          where usuario.UsuarioAcceso == usuarioDto.UsuarioAcceso && usuario.ClaveAcceso == usuarioDto.ClaveAcceso
                          select new UsuarioRol()
                          {
                              NombreUsuario = usuario.NombreUsuario,
                              UsuarioAcceso = usuario.UsuarioAcceso,
                              NombreRol = rol.NombreRol
                          }).FirstOrDefaultAsync();
        }

        public async Task<IList<UsuarioRol>> DevolverUsuarios()
        {
            return await (from usuario in _context.Usuarios
                          join role in _context.Roles on usuario.FkRol equals role.IdRol
                          select new UsuarioRol()
                          {
                              NombreUsuario = usuario.NombreUsuario,
                              UsuarioAcceso = usuario.UsuarioAcceso,
                              NombreRol = role.NombreRol,
                              IdUsuario = usuario.IdUsuario

                          }).ToListAsync();
        }

        public async Task<Usuario> EditarUsuario(Usuario usuario, int usuarioId)
        {
            if (usuario == null) return null;

            usuario.IdUsuario = usuarioId;
            _context.Usuarios.Attach(usuario);
            if (!string.IsNullOrEmpty(usuario.NombreUsuario))
            {
                _context.Entry(usuario).Property(u => u.NombreUsuario).IsModified = true;
            }

            if (!string.IsNullOrEmpty(usuario.UsuarioAcceso))
            {
                _context.Entry(usuario).Property(u => u.UsuarioAcceso).IsModified = true;
            }

            if (!string.IsNullOrEmpty(usuario.ClaveAcceso))
            {
                _context.Entry(usuario).Property(u => u.ClaveAcceso).IsModified = true;
            }

            if (usuario.FkRol > 0)
            {
                _context.Entry(usuario).Property(u => u.FkRol).IsModified = true;
            }

            await _context.SaveChangesAsync();
            return usuario;
        }
    }
}
