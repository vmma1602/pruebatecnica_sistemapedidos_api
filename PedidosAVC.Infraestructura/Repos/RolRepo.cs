﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Infraestructura.Datos;

namespace PedidosAVC.Infraestructura.Repos
{
    public class RolRepo : IRolRepo
    {
        private readonly PedidosDbContext _context;
        public RolRepo(PedidosDbContext context)
        {
            _context = context;
        }

        public async Task<IList<Role>> DevolverRoles()
        {
            return await _context.Roles.ToListAsync();
        }
    }
}
