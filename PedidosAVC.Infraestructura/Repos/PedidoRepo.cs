﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Infraestructura.Datos;

namespace PedidosAVC.Infraestructura.Repos
{
    public class PedidoRepo : IPedidoRepo
    {
        private readonly PedidosDbContext _context;
        public PedidoRepo(PedidosDbContext context)
        {
            _context = context;
        }

        public async Task<IList<PedidoProducto>> DevolverPedidos()
        {
            return await (from pedido in _context.Pedidos
                          join producto in _context.Productos on pedido.FkProducto equals producto.IdProducto
                          join cliente in _context.Clientes on pedido.FkCliente equals cliente.IdCliente
                          select new PedidoProducto()
                          {
                              IdCliente = cliente.IdCliente,
                              NombreCliente = cliente.NombreCliente,
                              IdProducto = producto.IdProducto,
                              IdPedido = pedido.IdPedido,
                              NombreProducto = producto.NombreProducto,
                              Estatus = pedido.Estatus,
                              Cantidad = pedido.Cantidad,
                              PrecioUnitario = producto.PrecioUnitario,
                              ClaveProducto = producto.ClaveProducto,
                              Descripcion = producto.Descripcion
                          })
                          .ToListAsync();
        }

        public async Task<Pedido> AgregarPedido(Pedido pedido)
        {
            await _context.Pedidos.AddAsync(pedido);
            await _context.SaveChangesAsync();
            int ultimoId = _context.Pedidos.Max(u => u.IdPedido);
            return await _context.Pedidos.Where(u => u.IdPedido == ultimoId).FirstOrDefaultAsync();
        }

        public async Task<Pedido> EditarPedidoPorId(Pedido pedido, int pedidoId)
        {
            if (pedido == null) return null;

            pedido.IdPedido = pedidoId;
            _context.Pedidos.Attach(pedido);

            if (!string.IsNullOrEmpty(pedido.Estatus))
            {
                _context.Entry(pedido).Property(u => u.Estatus).IsModified = true;
            }

            if (pedido.Cantidad > 0)
            {
                _context.Entry(pedido).Property(u => u.Cantidad).IsModified = true;
            }

            if (pedido.FkProducto > 0)
            {
                _context.Entry(pedido).Property(u => u.FkProducto).IsModified = true;
            }

            if (pedido.FkCliente > 0)
            {
                _context.Entry(pedido).Property(u => u.FkCliente).IsModified = true;
            }

            await _context.SaveChangesAsync();
            return pedido;
        }

        public async Task<Pedido> DevolverPedidoPorId(int pedidoId)
        {
            return await _context.Pedidos.Where(p => p.IdPedido == pedidoId).FirstOrDefaultAsync();
        }
    }
}
