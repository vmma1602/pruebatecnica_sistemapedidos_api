﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Infraestructura.Datos;

namespace PedidosAVC.Infraestructura.Repos
{
    public class ExistenciaRepo : IExistenciaRepo
    {
        private readonly PedidosDbContext _context;
        public ExistenciaRepo(PedidosDbContext context)
        {
            _context = context;
        }
        public async Task<IList<ProductoExistencia>> DevolverExistencias()
        {
            return await (from existencia in _context.Existencias
                          join producto in _context.Productos on existencia.FkProducto equals producto.IdProducto
                          select new ProductoExistencia()
                          {
                              NombreProducto = producto.NombreProducto,
                              ClaveProducto = producto.ClaveProducto,
                              Stock = existencia.Stock,
                              IdExistencia = existencia.IdExistencia,
                              IdProducto = producto.IdProducto,
                              PrecioUnitario = producto.PrecioUnitario,
                              UnidadDeMedida = existencia.UnidadDeMedida
                          })
                          .ToListAsync();
        }
        public async Task<ProductoExistencia> DevolverExistenciaPorProductoId(int productoId)
        {
            return await (from existencia in _context.Existencias
                          join producto in _context.Productos on existencia.FkProducto equals producto.IdProducto
                          where producto.IdProducto == productoId
                          select new ProductoExistencia()
                          {
                              NombreProducto = producto.NombreProducto,
                              ClaveProducto = producto.ClaveProducto,
                              Stock = existencia.Stock,
                              IdExistencia = existencia.IdExistencia,
                              IdProducto = producto.IdProducto,
                              PrecioUnitario = producto.PrecioUnitario,
                              UnidadDeMedida = existencia.UnidadDeMedida
                          })
                          .FirstOrDefaultAsync();
        }

        public async Task<Existencia> EditarExistencia(Existencia existencia, int existenciaId)
        {
            if (existencia == null) return null;

            existencia.IdExistencia = existenciaId;
            _context.Existencias.Attach(existencia);
            if (!string.IsNullOrEmpty(existencia.UnidadDeMedida))
            {
                _context.Entry(existencia).Property(e => e.UnidadDeMedida).IsModified = true;
            }

            _context.Entry(existencia).Property(e => e.Stock).IsModified = true;


            await _context.SaveChangesAsync();
            return existencia;
        }
    }
}
