﻿using Microsoft.EntityFrameworkCore;
using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Infraestructura.Datos
{
    public partial class PedidosDbContext : DbContext
    {
        //Se hizo Scaffold de la base de datos (Database First)
        public PedidosDbContext()
        {
        }

        public PedidosDbContext(DbContextOptions<PedidosDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; } = null!;
        public virtual DbSet<Existencia> Existencias { get; set; } = null!;
        public virtual DbSet<Pedido> Pedidos { get; set; } = null!;
        public virtual DbSet<Producto> Productos { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Usuario> Usuarios { get; set; } = null!;


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.IdCliente)
                    .HasName("PK__clientes__885457EE790D8155");

                entity.ToTable("clientes");

                entity.Property(e => e.IdCliente).HasColumnName("idCliente");

                entity.Property(e => e.NombreCliente)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nombreCliente");
            });

            modelBuilder.Entity<Existencia>(entity =>
            {
                entity.HasKey(e => e.IdExistencia)
                    .HasName("PK__existenc__70F8778B1963BB7C");

                entity.ToTable("existencias");

                entity.Property(e => e.IdExistencia).HasColumnName("idExistencia");

                entity.Property(e => e.FkProducto).HasColumnName("fkProducto");

                entity.Property(e => e.Stock).HasColumnName("stock");

                entity.Property(e => e.UnidadDeMedida)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("unidadDeMedida");

                entity.HasOne(d => d.FkProductoNavigation)
                    .WithMany(p => p.Existencia)
                    .HasForeignKey(d => d.FkProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__existenci__fkPro__403A8C7D");
            });

            modelBuilder.Entity<Pedido>(entity =>
            {
                entity.HasKey(e => e.IdPedido)
                    .HasName("PK__pedidos__A9F619B78E1FE2E0");

                entity.ToTable("pedidos");

                entity.Property(e => e.IdPedido).HasColumnName("idPedido");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.FkCliente).HasColumnName("fkCliente");

                entity.Property(e => e.FkProducto).HasColumnName("fkProducto");

                entity.Property(e => e.Estatus).HasColumnName("estatus");

                entity.HasOne(d => d.FkClienteNavigation)
                    .WithMany(p => p.Pedidos)
                    .HasForeignKey(d => d.FkCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__pedidos__fkClien__440B1D61");

                entity.HasOne(d => d.FkProductoNavigation)
                    .WithMany(p => p.Pedidos)
                    .HasForeignKey(d => d.FkProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__pedidos__fkProdu__4316F928");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProducto)
                    .HasName("PK__producto__07F4A1320B0C0D19");

                entity.ToTable("productos");

                entity.Property(e => e.IdProducto).HasColumnName("idProducto");

                entity.Property(e => e.ClaveProducto)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("claveProducto");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.NombreProducto)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nombreProducto");

                entity.Property(e => e.PrecioUnitario).HasColumnName("precioUnitario");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.IdRol)
                    .HasName("PK__roles__3C872F766A4EF860");

                entity.ToTable("roles");

                entity.Property(e => e.IdRol).HasColumnName("idRol");

                entity.Property(e => e.NombreRol)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombreRol");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PK__usuarios__645723A6DD946EA7");

                entity.ToTable("usuarios");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.ClaveAcceso)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("claveAcceso");

                entity.Property(e => e.FkRol).HasColumnName("fkRol");

                entity.Property(e => e.NombreUsuario)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombreUsuario");

                entity.Property(e => e.UsuarioAcceso)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("usuarioAcceso");

                entity.HasOne(d => d.FkRolNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.FkRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__usuarios__fkRol__398D8EEE");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
