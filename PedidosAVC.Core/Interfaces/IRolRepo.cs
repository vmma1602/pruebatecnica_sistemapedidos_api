﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IRolRepo
    {
        public Task<IList<Role>> DevolverRoles();
    }
}
