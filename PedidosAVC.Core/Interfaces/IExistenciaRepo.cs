﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IExistenciaRepo
    {
        public Task<IList<ProductoExistencia>> DevolverExistencias();

        public Task<Existencia> EditarExistencia(Existencia existencia, int existenciaId);

        public Task<ProductoExistencia> DevolverExistenciaPorProductoId(int productoId);
    }
}
