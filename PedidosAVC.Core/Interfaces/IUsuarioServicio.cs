﻿using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IUsuarioServicio
    {
        public Task<IList<UsuarioRol>> DevolverUsuarios();

        public Task<UsuarioRol> BuscarUsuarioParaLogin(UsuarioDto usuarioDto);

        public Task<Usuario> AgregarUsuario(Usuario usuario);

        public Task<Usuario> EditarUsuario(Usuario usuario, int usuarioId);
    }
}
