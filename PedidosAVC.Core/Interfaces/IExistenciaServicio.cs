﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IExistenciaServicio
    {
        public Task<IList<ProductoExistencia>> DevolverExistencias();

        public Task<Existencia> EditarExistencia(Existencia existencia, int existenciaId);
        public Task<ProductoExistencia> DevolverExistenciaPorProductoId(int productoId);
    }
}
