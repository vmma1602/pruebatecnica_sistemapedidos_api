﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IClienteServicio
    {
        public Task<IList<Cliente>> DevolverClientes();
    }
}
