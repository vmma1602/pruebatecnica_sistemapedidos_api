﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IClienteRepo
    {
        public Task<IList<Cliente>> DevolverClientes();
    }
}
