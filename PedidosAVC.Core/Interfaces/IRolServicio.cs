﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IRolServicio
    {
        public Task<IList<Role>> DevolverRoles();
    }
}
