﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IPedidoRepo
    {
        public Task<Pedido> EditarPedidoPorId(Pedido pedido, int pedidoId);
        public Task<IList<PedidoProducto>> DevolverPedidos();
        public Task<Pedido> AgregarPedido(Pedido pedido);
        public Task<Pedido> DevolverPedidoPorId(int pedidoId);
    }
}
