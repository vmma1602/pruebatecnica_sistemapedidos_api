﻿using PedidosAVC.Core.Entidades;

namespace PedidosAVC.Core.Interfaces
{
    public interface IProductoRepo
    {
        public Task<IList<Producto>> DevolverProductos();

        public Task<Producto> AgregarProducto(Producto producto);

        public Task<Producto> EditarProducto(Producto producto, int productoId);
    }
}
