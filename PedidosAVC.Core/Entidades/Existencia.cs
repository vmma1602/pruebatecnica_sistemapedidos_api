﻿namespace PedidosAVC.Core.Entidades
{
    public partial class Existencia
    {
        public int IdExistencia { get; set; }
        public string? UnidadDeMedida { get; set; }
        public double? Stock { get; set; }
        public int FkProducto { get; set; }

        public virtual Producto FkProductoNavigation { get; set; } = null!;
    }
}
