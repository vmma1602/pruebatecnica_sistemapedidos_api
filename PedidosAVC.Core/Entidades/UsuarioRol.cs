﻿namespace PedidosAVC.Core.Entidades
{
    public class UsuarioRol : Usuario
    {
        public string NombreRol { get; set; }
    }
}
