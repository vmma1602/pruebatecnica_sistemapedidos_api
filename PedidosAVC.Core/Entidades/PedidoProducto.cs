﻿namespace PedidosAVC.Core.Entidades
{
    public class PedidoProducto : Producto
    {
        //Cliente
        public int IdCliente { get; set; }
        public string NombreCliente { get; set; }

        //Pedido
        public int IdPedido { get; set; }
        public double? Cantidad { get; set; }
        public string Estatus { get; set; }
    }
}
