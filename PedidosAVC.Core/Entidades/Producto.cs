﻿namespace PedidosAVC.Core.Entidades
{
    public partial class Producto
    {
        public Producto()
        {
            Existencia = new HashSet<Existencia>();
            Pedidos = new HashSet<Pedido>();
        }

        public int IdProducto { get; set; }
        public string? ClaveProducto { get; set; }
        public string NombreProducto { get; set; } = null!;
        public string Descripcion { get; set; } = null!;
        public double? PrecioUnitario { get; set; }

        public virtual ICollection<Existencia> Existencia { get; set; }
        public virtual ICollection<Pedido> Pedidos { get; set; }
    }
}
