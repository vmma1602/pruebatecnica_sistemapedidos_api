﻿namespace PedidosAVC.Core.Entidades
{
    public partial class Pedido
    {
        public int IdPedido { get; set; }
        public double? Cantidad { get; set; }
        public string Estatus { get; set; }

        public int FkProducto { get; set; }
        public int FkCliente { get; set; }

        public virtual Cliente FkClienteNavigation { get; set; } = null!;
        public virtual Producto FkProductoNavigation { get; set; } = null!;
    }
}
