﻿namespace PedidosAVC.Core.Entidades
{
    public partial class Usuario
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; } = null!;
        public string UsuarioAcceso { get; set; } = null!;
        public string ClaveAcceso { get; set; } = null!;
        public int FkRol { get; set; }

        public virtual Role FkRolNavigation { get; set; } = null!;
    }
}
