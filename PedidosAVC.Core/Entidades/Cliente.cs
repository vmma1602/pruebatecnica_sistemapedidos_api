﻿namespace PedidosAVC.Core.Entidades
{
    public partial class Cliente
    {
        public Cliente()
        {
            Pedidos = new HashSet<Pedido>();
        }

        public int IdCliente { get; set; }
        public string NombreCliente { get; set; } = null!;

        public virtual ICollection<Pedido> Pedidos { get; set; }
    }
}
