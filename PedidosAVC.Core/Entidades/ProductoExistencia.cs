﻿namespace PedidosAVC.Core.Entidades
{
    public class ProductoExistencia : Producto
    {
        public int IdExistencia { get; set; }
        public string UnidadDeMedida { get; set; } = null!;
        public double? Stock { get; set; }
    }
}
