﻿namespace PedidosAVC.Core.Dtos
{
    public class ProductoExistenciaDto
    {
        public int IdExistencia { get; set; }
        public string? ClaveProducto { get; set; }
        public string? NombreProducto { get; set; }
        public double? PrecioUnitario { get; set; }
        public string? UnidadDeMedida { get; set; }
        public double? Stock { get; set; }
    }
}
