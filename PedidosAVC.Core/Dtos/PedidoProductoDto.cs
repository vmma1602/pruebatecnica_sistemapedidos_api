﻿namespace PedidosAVC.Core.Dtos
{
    public class PedidoProductoDto
    {
        //Cliente
        public int IdCliente { get; set; }
        public string? NombreCliente { get; set; }

        //Pedido
        public int IdPedido { get; set; }
        public double? Cantidad { get; set; }
        public string? Estatus { get; set; }

        //Producto
        public int IdProducto { get; set; }
        public string? ClaveProducto { get; set; }
        public string? NombreProducto { get; set; }
        public string? Descripcion { get; set; }
        public double? PrecioUnitario { get; set; }
    }
}
