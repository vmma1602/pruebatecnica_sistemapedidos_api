﻿namespace PedidosAVC.Core.Dtos
{
    public class ProductoDto
    {
        public int IdProducto { get; set; }
        public string? ClaveProducto { get; set; }
        public string? NombreProducto { get; set; }
        public string? Descripcion { get; set; }
        public double? PrecioUnitario { get; set; }
    }
}
