﻿namespace PedidosAVC.Core.Dtos
{
    public class ClienteDto
    {
        public int IdCliente { get; set; }
        public string NombreCliente { get; set; }
    }
}
