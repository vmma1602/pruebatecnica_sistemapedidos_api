﻿namespace PedidosAVC.Core.Dtos
{
    public class PedidoDto
    {
        public int IdPedido { get; set; }
        public double? Cantidad { get; set; }
        public string Estatus { get; set; }

        public int FkProducto { get; set; }
        public int FkCliente { get; set; }
    }
}
