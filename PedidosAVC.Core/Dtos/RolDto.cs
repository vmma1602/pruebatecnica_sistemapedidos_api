﻿namespace PedidosAVC.Core.Dtos
{
    public class RolDto
    {
        public int IdRol { get; set; }
        public string? NombreRol { get; set; }
    }
}
