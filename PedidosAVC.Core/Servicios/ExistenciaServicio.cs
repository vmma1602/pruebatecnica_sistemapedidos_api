﻿using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Core.Servicios
{
    public class ExistenciaServicio : IExistenciaServicio
    {
        private readonly IExistenciaRepo _existenciaRepo;
        public ExistenciaServicio(IExistenciaRepo existenciaRepo)
        {
            _existenciaRepo = existenciaRepo;
        }

        public async Task<ProductoExistencia> DevolverExistenciaPorProductoId(int productoId)
        {
            return await _existenciaRepo.DevolverExistenciaPorProductoId(productoId);
        }

        public async Task<IList<ProductoExistencia>> DevolverExistencias()
        {
            return await _existenciaRepo.DevolverExistencias();
        }

        public async Task<Existencia> EditarExistencia(Existencia existencia, int existenciaId)
        {
            return await _existenciaRepo.EditarExistencia(existencia, existenciaId);
        }


    }
}
