﻿using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Core.Servicios
{
    public class PedidoServicio : IPedidoServicio
    {
        private readonly IPedidoRepo _pedidoRepo;

        public PedidoServicio(IPedidoRepo pedidoRepo)
        {
            _pedidoRepo = pedidoRepo;
        }
        public async Task<IList<PedidoProducto>> DevolverPedidos()
        {
            return await _pedidoRepo.DevolverPedidos();
        }

        public async Task<Pedido> AgregarPedido(Pedido pedido)
        {
            return await _pedidoRepo.AgregarPedido(pedido);
        }

        public async Task<Pedido> DevolverPedidoPorId(int pedidoId)
        {
            return await _pedidoRepo.DevolverPedidoPorId(pedidoId);
        }

        public async Task<Pedido> EditarPedidoPorId(Pedido pedido, int pedidoId)
        {
            return await _pedidoRepo.EditarPedidoPorId(pedido, pedidoId);
        }
    }
}
