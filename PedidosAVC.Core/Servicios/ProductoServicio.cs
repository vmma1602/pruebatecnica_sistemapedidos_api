﻿
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Core.Servicios
{
    public class ProductoServicio : IProductoServicio
    {
        private readonly IProductoRepo _productoRepo;
        public ProductoServicio(IProductoRepo productoRepo)
        {
            _productoRepo = productoRepo;
        }

        public async Task<Producto> AgregarProducto(Producto producto)
        {
            return await _productoRepo.AgregarProducto(producto);
        }

        public async Task<IList<Producto>> DevolverProductos()
        {
            return await _productoRepo.DevolverProductos();
        }

        public async Task<Producto> EditarProducto(Producto producto, int productoId)
        {
            return await _productoRepo.EditarProducto(producto, productoId);
        }
    }
}
