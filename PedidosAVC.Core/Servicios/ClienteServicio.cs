﻿using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Core.Servicios
{
    public class ClienteServicio : IClienteServicio
    {
        private readonly IClienteRepo _clienteRepo;
        public ClienteServicio(IClienteRepo clienteRepo)
        {
            _clienteRepo = clienteRepo;
        }

        public async Task<IList<Cliente>> DevolverClientes()
        {
            return await _clienteRepo.DevolverClientes();
        }
    }
}
