﻿using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Core.Servicios
{

    public class UsuarioServicio : IUsuarioServicio
    {
        private readonly IUsuarioRepo _usuarioRepo;
        public UsuarioServicio(IUsuarioRepo usuarioRepo)
        {
            _usuarioRepo = usuarioRepo;
        }

        public async Task<Usuario> AgregarUsuario(Usuario usuario)
        {
            return await _usuarioRepo.AgregarUsuario(usuario);
        }

        public async Task<UsuarioRol> BuscarUsuarioParaLogin(UsuarioDto usuarioDto)
        {
            return await _usuarioRepo.BuscarUsuarioParaLogin(usuarioDto);
        }

        public async Task<IList<UsuarioRol>> DevolverUsuarios()
        {
            return await _usuarioRepo.DevolverUsuarios();
        }

        public async Task<Usuario> EditarUsuario(Usuario usuario, int usuarioId)
        {
            return await _usuarioRepo.EditarUsuario(usuario, usuarioId);
        }
    }
}
