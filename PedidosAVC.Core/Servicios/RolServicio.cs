﻿using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Core.Servicios
{
    public class RolServicio : IRolServicio
    {

        private readonly IRolRepo _rolRepo;
        public RolServicio(IRolRepo rolRepo)
        {
            _rolRepo = rolRepo;
        }

        public async Task<IList<Role>> DevolverRoles()
        {
            return await _rolRepo.DevolverRoles();
        }
    }
}
