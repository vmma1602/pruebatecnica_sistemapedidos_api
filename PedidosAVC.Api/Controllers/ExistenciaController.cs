﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExistenciaController : ControllerBase
    {
        private readonly IExistenciaServicio _existenciaServicio;
        private readonly IMapper _mapper;
        public ExistenciaController(IExistenciaServicio existenciaServicio, IMapper mapper)
        {
            _existenciaServicio = existenciaServicio;
            _mapper = mapper;
        }

        [HttpGet("DevolverExistencias")]
        public async Task<IActionResult> DevolverExistencias()
        {
            var exists = await _existenciaServicio.DevolverExistencias();
            if (exists == null) return BadRequest();

            var existenciasDto = _mapper.Map<IList<ProductoExistenciaDto>>(exists);
            return Ok(existenciasDto);
        }

        [HttpPut("EditarExistencia")]
        public async Task<IActionResult> EditarExistencia([FromBody] ProductoExistenciaDto existenciaDto, [FromQuery] int existenciaId)
        {
            var existencia = _mapper.Map<Existencia>(existenciaDto);
            var editada = await _existenciaServicio.EditarExistencia(existencia, existenciaId);
            if (editada == null) return BadRequest();

            existenciaDto = _mapper.Map<ProductoExistenciaDto>(editada);
            return Ok(existenciaDto);
        }

        [HttpGet("DevolverExistenciaPorProductoId")]
        public async Task<IActionResult> DevolverExistenciaPorProductoId([FromQuery] int productoId)
        {
            var existencia = await _existenciaServicio.DevolverExistenciaPorProductoId(productoId);
            if (existencia == null) return BadRequest();

            var existenciaDto = _mapper.Map<ProductoExistenciaDto>(existencia);
            return Ok(existenciaDto);
        }
    }
}
