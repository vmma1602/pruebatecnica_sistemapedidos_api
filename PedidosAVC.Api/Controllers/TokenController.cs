﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuracion;
        private readonly IUsuarioServicio _usuarioServicio;



        public TokenController(IConfiguration configuracion, IUsuarioServicio usuarioServicio)
        {
            _configuracion = configuracion;
            _usuarioServicio = usuarioServicio;

        }
        [HttpPost("Autenticar")]
        public async Task<IActionResult> Autenticar([FromBody] UsuarioDto usuarioDto)
        {
            var usuarioRol = await _usuarioServicio.BuscarUsuarioParaLogin(usuarioDto);
            if (usuarioRol == null) return BadRequest();
            string token = GenerarToken(usuarioRol);
            var response = new
            {
                usuarioRol.NombreUsuario,
                usuarioRol.UsuarioAcceso,
                Rol = usuarioRol.NombreRol,
                token
            };
            return Ok(response);
        }

        private string GenerarToken(UsuarioRol usuarioRol)
        {
            //Header
            var claveSimetrica = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuracion["Autenticacion:ClaveSecreta"]));
            var credenciales = new SigningCredentials(claveSimetrica, SecurityAlgorithms.HmacSha256);
            var header = new JwtHeader(credenciales);

            //Claims
            var claims = new List<Claim>();

            claims.Add(new Claim("role", usuarioRol.NombreRol));  //El token lleva los roles
            claims.Add(new Claim("nombreUsuario", usuarioRol.NombreUsuario));
            claims.Add(new Claim("usuario", usuarioRol.UsuarioAcceso));

            //Payload
            var payload = new JwtPayload(
                  _configuracion["Autenticacion:Editor"],
                  _configuracion["Autenticacion:Audiencia"],
                  claims,
                  DateTime.Now,
                  DateTime.Now.AddDays(1)
                  );


            //Token
            var token = new JwtSecurityToken(header, payload);
            return new JwtSecurityTokenHandler().WriteToken(token);  //Es string
        }
    }
}
