﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;
namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private readonly IPedidoServicio _pedidoServicio;
        private readonly IMapper _mapper;
        public PedidoController(IPedidoServicio pedidoServicio, IMapper mapper)
        {
            _pedidoServicio = pedidoServicio;
            _mapper = mapper;
        }

        [HttpGet("DevolverPedidos")]
        public async Task<IActionResult> DevolverExistencias()
        {
            var pedidos = await _pedidoServicio.DevolverPedidos();
            if (pedidos == null) return BadRequest();

            var pedidosDto = _mapper.Map<IList<PedidoProductoDto>>(pedidos);
            return Ok(pedidosDto);
        }

        [HttpPost("AgregarPedido")]
        public async Task<IActionResult> AgregarPedido([FromBody] PedidoDto pedidoDto)
        {
            var pedido = _mapper.Map<Pedido>(pedidoDto);
            pedido = await _pedidoServicio.AgregarPedido(pedido);
            if (pedido == null) return BadRequest();

            pedidoDto = _mapper.Map<PedidoDto>(pedido);
            return Ok(pedidoDto);
        }

        [HttpPut("EditarPedido")]
        public async Task<IActionResult> EditarPedido([FromBody] PedidoDto pedidoDto, [FromQuery] int pedidoId)
        {
            var pedido = _mapper.Map<Pedido>(pedidoDto);
            var pedidoEditado = await _pedidoServicio.EditarPedidoPorId(pedido, pedidoId);
            if (pedidoEditado == null) return BadRequest();

            pedidoDto = _mapper.Map<PedidoDto>(pedidoEditado);
            return Ok(pedidoDto);
        }

        [HttpGet("DevolverPedidoPorId")]
        public async Task<IActionResult> DevolverPedidoPorId([FromQuery] int pedidoId)
        {
            var pedido = await _pedidoServicio.DevolverPedidoPorId(pedidoId);
            if (pedido == null) return BadRequest();

            var pedidoDto = _mapper.Map<PedidoDto>(pedido);
            return Ok(pedidoDto);
        }
    }
}
