﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteServicio _clienteServicio;
        private readonly IMapper _mapper;
        public ClienteController(IClienteServicio clienteServicio, IMapper mapper)
        {
            _clienteServicio = clienteServicio;
            _mapper = mapper;
        }

        [HttpGet("DevolverClientes")]
        public async Task<IActionResult> DevolverClientes()
        {
            var clientes = await _clienteServicio.DevolverClientes();
            if (clientes == null) return BadRequest();

            var clientesDto = _mapper.Map<IList<ClienteDto>>(clientes);
            return Ok(clientesDto);
        }
    }
}
