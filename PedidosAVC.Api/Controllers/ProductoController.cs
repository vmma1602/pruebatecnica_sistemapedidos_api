﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly IProductoServicio _productoServicio;
        private readonly IMapper _mapper;
        public ProductoController(IProductoServicio productoServicio, IMapper mapper)
        {
            _productoServicio = productoServicio;
            _mapper = mapper;
        }

        [HttpGet("DevolverProductos")]
        public async Task<IActionResult> DevolverProductos()
        {
            var productos = await _productoServicio.DevolverProductos();
            if (productos == null) return BadRequest();

            var productosDto = _mapper.Map<IList<ProductoDto>>(productos);
            return Ok(productosDto);
        }

        [HttpPost("AgregarProducto")]
        public async Task<IActionResult> AgregarProducto([FromBody] ProductoDto productoDto)
        {
            var producto = _mapper.Map<Producto>(productoDto);
            var productoAgregado = await _productoServicio.AgregarProducto(producto);
            if (productoAgregado == null) return BadRequest();

            productoDto = _mapper.Map<ProductoDto>(productoAgregado);
            return Ok(productoDto);
        }


        [HttpPut("EditarProducto")]
        public async Task<IActionResult> EditarProducto([FromBody] ProductoDto productoDto, [FromQuery] int productoId)
        {
            var producto = _mapper.Map<Producto>(productoDto);
            var productoEditado = await _productoServicio.EditarProducto(producto, productoId);
            if (productoEditado == null) return BadRequest();

            productoDto = _mapper.Map<ProductoDto>(productoEditado);
            return Ok(productoDto);
        }
    }
}
