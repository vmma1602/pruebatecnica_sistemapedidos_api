﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Interfaces;
namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolController : ControllerBase
    {
        private readonly IRolServicio _rolServicio;
        private readonly IMapper _mapper;
        public RolController(IRolServicio rolServicio, IMapper mapper)
        {
            _rolServicio = rolServicio;
            _mapper = mapper;
        }

        [HttpGet("DevolverRoles")]
        public async Task<IActionResult> DevolverRoles()
        {
            var roles = await _rolServicio.DevolverRoles();
            if (roles == null) return BadRequest();

            var rolesDto = _mapper.Map<IList<RolDto>>(roles);
            return Ok(rolesDto);
        }
    }
}
