﻿using AutoMapper;

using Microsoft.AspNetCore.Mvc;
using PedidosAVC.Core.Dtos;
using PedidosAVC.Core.Entidades;
using PedidosAVC.Core.Interfaces;

namespace PedidosAVC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioServicio _usuarioServicio;
        private readonly IMapper _mapper;
        public UsuarioController(IUsuarioServicio usuarioServicio, IMapper mapper)
        {
            _usuarioServicio = usuarioServicio;
            _mapper = mapper;
        }

        [HttpGet("DevolverUsuarios")]
        public async Task<IActionResult> DevolverUsuarios()
        {
            var usuarios = await _usuarioServicio.DevolverUsuarios();
            if (usuarios == null) return BadRequest();

            var usuariosDto = _mapper.Map<IList<UsuarioDto>>(usuarios);
            return Ok(usuariosDto);
        }

        [HttpPost("AgregarUsuario")]
        public async Task<IActionResult> AgregarUsuario([FromBody] UsuarioDto usuarioDto)
        {
            var usuario = _mapper.Map<Usuario>(usuarioDto);
            var usuarioAgregado = await _usuarioServicio.AgregarUsuario(usuario);
            if (usuarioAgregado == null) return BadRequest();

            usuarioDto = _mapper.Map<UsuarioDto>(usuarioAgregado);
            return Ok(usuarioDto);
        }


        [HttpPut("EditarUsuario")]
        public async Task<IActionResult> EditarUsuario([FromBody] UsuarioDto usuarioDto, [FromQuery] int usuarioId)
        {
            var usuario = _mapper.Map<Usuario>(usuarioDto);
            var usuarioEditado = await _usuarioServicio.EditarUsuario(usuario, usuarioId);
            if (usuarioEditado == null) return BadRequest();

            usuarioDto = _mapper.Map<UsuarioDto>(usuarioEditado);
            return Ok(usuarioDto);
        }


    }
}
