using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PedidosAVC.Core.Interfaces;
using PedidosAVC.Core.Servicios;
using PedidosAVC.Infraestructura.Datos;
using PedidosAVC.Infraestructura.Repos;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//DI
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddTransient<IUsuarioRepo, UsuarioRepo>();
builder.Services.AddTransient<IUsuarioServicio, UsuarioServicio>();
builder.Services.AddTransient<IRolRepo, RolRepo>();
builder.Services.AddTransient<IRolServicio, RolServicio>();
builder.Services.AddTransient<IClienteRepo, ClienteRepo>();
builder.Services.AddTransient<IClienteServicio, ClienteServicio>();
builder.Services.AddTransient<IProductoRepo, ProductoRepo>();
builder.Services.AddTransient<IProductoServicio, ProductoServicio>();
builder.Services.AddTransient<IExistenciaRepo, ExistenciaRepo>();
builder.Services.AddTransient<IExistenciaServicio, ExistenciaServicio>();
builder.Services.AddTransient<IPedidoRepo, PedidoRepo>();
builder.Services.AddTransient<IPedidoServicio, PedidoServicio>();

builder.Services.AddDbContext<PedidosDbContext>(
            dbContextOptions => dbContextOptions
                .UseSqlServer(builder.Configuration.GetConnectionString("sistemaPedidos")), ServiceLifetime.Transient);

//Autenticacion
builder.Services
    .AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

    }).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters()  //No se incluy� en la peticiones
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateIssuerSigningKey = true,
            ValidateLifetime = true,
            ValidIssuer = builder.Configuration["Autenticacion:Editor"],
            ValidAudience = builder.Configuration["Autenticacion:Audiencia"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Autenticacion:ClaveSecreta"]))
        };
    });
//Autenticacion


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseCors(options =>
{
    options
     .AllowAnyOrigin()   //Se deshabilita el CORS para facilitar las pruebas
     .AllowAnyMethod()
     .AllowAnyHeader();
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
